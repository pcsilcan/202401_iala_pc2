#include <iostream>
#include <iomanip>
#include <random>

using namespace std;

int randint(int min, int max, random_device& rd) {
    mt19937_64 gen(rd());
    return gen() % (max - min) + min;
}

void printArray(int* arr, int n) {
    cout << "[";
    for (int i = 0; i < n; ++i) {
        cout << setw(3) << arr[i];
    }
    cout << "]\n";
}

void swap(int* a, int* b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

void bubbleSort(int* arr, int n) {
    //for (int i = 0; i < n - 1; ++i) {
    //    for (int j = 0; j < n - 1 - i; ++j) {
    //        if (arr[j] > arr[j + 1]) {
    //            int temp = arr[j];
    //            arr[j] = arr[j + 1];
    //            arr[j + 1] = temp;
    //        }
    //    }
    //}
    for (int i = 0; i < n - 1; ++i) {
        swap(arr[i], arr[i + 1]);
    }
    arr[n] = arr[n - 1];
}

void deleteElement(int* arr, int n, int k) {
    for (int i = 0; i < n; ++i) {
        if (i == k) {
            arr[i] = 0;
        }        
    }
}

int main() {
    random_device rd;
    int n;
    int *indice= new int;
	int* arr = new int[n];
    
    cout << "Ingrese n: ";
    cin >> n;
    for (int i = 0; i < n; ++i) {
        arr[i] = randint(0,100, rd);
    }
    printArray(arr, n);

    cout << "ingrese la ubicacion del elemento a eliminar: ";
    cin >> *indice;

    deleteElement(arr, n, *indice);
   
    //bubbleSort(arr, n);
    printArray(arr, n);
    system("pause");
    return EXIT_SUCCESS;
}