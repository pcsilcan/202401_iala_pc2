#include <iostream>
#include <cmath>
using namespace std;

bool esPrimo(int numero) {
    if (numero <= 1) {
        return false;
    }
    if (numero == 2) {
        return true;
    }
    if (numero % 2 == 0) {
        return false;
    }
    for (int i = 3; i <= sqrt(numero); i += 2) {
        if (numero % i == 0) {
            return false;
        }
    }
    return true;
}

int main() {
    int numero;
    cout << "Ingrese un numero entero positivo: ";
    cin >> numero;

    int* divisores = new int[5]; 
    bool* esDivisible = new bool[5]; 

    cout << "Tabla de Divisibilidad:" <<endl;
    cout << "Divisor\t|\tDivisible?" << endl;
    cout << "-----------------------------" << endl;
    int* pDivisores = divisores;
    bool* pEsDivisible = esDivisible;
    for (int i = 2; i <= 6; ++i) {
        *pDivisores = i;
        *pEsDivisible = (numero % i == 0);
        cout << *pDivisores << "\t|\t" << (*pEsDivisible ? "Si" : "No") << endl;
        pDivisores++;
        pEsDivisible++;
    }
    cout << "-----------------------------" << endl;

    bool primo = esPrimo(numero);

    if (primo) {
        cout << numero << " es primo." << endl;
    }
    else {
        cout << numero << " no es primo." << endl;
    }

    delete[] divisores;
    delete[] esDivisible;

    system("pause");
    return 0;
}
