#include <iostream>
#include <cmath>

using namespace std;

bool esPrimo(const int* numero) {
    if (*numero <= 1) {
        return false;
    }
    for (int i = 2; i <= sqrt(*numero); i++) {
        if (*numero % i == 0) {
            return false;
        }
    }
    return true;
}

int main() {
    int* ptrNum = new int;
    cout << "Ingrese un numero: ";
    cin >> *ptrNum;

    if (esPrimo(ptrNum)) {
        cout << *ptrNum << " es un numero primo" << endl;
    }
    else {
        cout << *ptrNum << " no es un numero primo" << endl;
    }

    delete ptrNum; 
     system("pause");

     return 0;
}
