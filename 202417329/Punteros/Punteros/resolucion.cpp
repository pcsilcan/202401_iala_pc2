#include <iostream>
#include <math.h>
#include <string>
using namespace std;
void checkPrimeNumber(int x) {
	int raiz = sqrt(x);
	int* array = new int[raiz];
	int todosno = 0;
	for (int i = 0; i <= raiz; ++i) {
		array[i] = i + 2;
	}
	string* yesorno = new string[raiz];
	for (int j = 0; j < raiz; ++j) {
		if (x % array[j] == 0) {
			yesorno[j] = "si";
		}
		else { yesorno[j] = "no"; }
	}
	cout << "Divisor  |  Divisible" << endl;
	for (int k = 0; k < raiz; ++k) {
		cout << array[k] << "  |  " << yesorno[k] << endl;
	}
	for (int l = 0; l < raiz; ++l) {
		if (yesorno[l] == "no") {
			++todosno;
		}
	}
	if (todosno == raiz) {
		cout << x << " es un numero primo";
	}
	else {
		cout << x << " no es un numero primo";
	}
	delete[] array;
	delete[] yesorno;
}
int main() {
	int r;
	cout << "Ingrese un numero para verificar si este es primo: "; cin >> r;
	checkPrimeNumber(r);
	return 0;
}