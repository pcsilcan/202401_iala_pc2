#include <iostream>

using namespace std;

int* criba(int N) {
    int* primos = new int[N + 1];

    for (int i = 0; i <= N; i++) {
        primos[i] = 1; 
    }

    primos[0] = primos[1] = 0;

    for (int i = 2; i * i <= N; i++) {
        if (primos[i]) {
            for (int j = i * i; j <= N; j += i) {
                primos[j] = 0; 
            }
        }
    }

    return primos;
}

void imprimirPrimos(int* primos, int N) {
    cout << "Los numeros primos entre 1 y " << N << " son:\n";
    cout << "NUMEROS   | PRIMOS" << endl;
    cout << "------------------" << endl;
    for (int i = 2; i <= N; i++) {
        cout << i << "         | " << (primos[i] ? "0" : "-1") << endl;
    }
}

int main() {
    int N;
    cout << "Ingrese un numero entero positivo: ";
    cin >> N;

    int* primos = criba(N);
    imprimirPrimos(primos, N);

    delete[] primos; 

    system("pause");
    return 0;
}


