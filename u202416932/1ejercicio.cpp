#include <iostream>
#include <cstdio>

using namespace std;

bool* generarPrimosHasta(int max) {
    bool* primos = new bool[max + 1]; 

    for (int i = 0; i <= max; i++) {
        primos[i] = true; 
    }

    primos[0] = primos[1] = false;

    for (int i = 2; i * i <= max; i++) {
        if (primos[i]) {
            for (int j = i * i; j <= max; j += i) {
                primos[j] = false; 
            }
        }
    }

    return primos;
}

bool es_primo(int numero, bool* primos) {
    return primos[numero]; 
}

int main() {
    int num;
    cout << "Ingrese un numero para verificar si es primo o no: ";
    cin >> num;

    bool* primos = generarPrimosHasta(num);
    if (es_primo(num, primos)) {
        cout << num << " es un numero primo." << endl;
    }
    else {
        cout << num << " no es un numero primo." << endl;
    }

    delete[] primos;

    system("pause");
    return 0;
}



